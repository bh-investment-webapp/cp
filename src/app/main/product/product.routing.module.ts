import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductListComponent as ListComponent } from './product-list/product-list.component';
import { ProductCreateComponent as CreateComponent } from './product-create/product-create.component';
import { ProductComponent as MainComponent } from './product.component';
import { ProductEditComponent as EditComponent} from './product-edit/product-edit.component';

const routes: Routes = [
    {
        path: '', component: MainComponent,
        children: [
            {
                path: '',
                // canActivateChild: [AuthGuard],
                children: [
                    { path: '', component: ListComponent },
                    { path: 'create', component: CreateComponent },
                    { path: ':id/overview', component: EditComponent },

                ]
            }
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ProductRoutingModule { }
