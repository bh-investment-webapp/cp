import { Component, OnInit, AfterViewInit, ViewChild, TemplateRef } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {  MatDialog } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Service as ServiceCls } from '../product-category';
import { ProductCategoryService as Service } from '../product-category.service';
import { MatSort } from '@angular/material';
import { TableColumnInfo } from 'app/shared/table/table.component';
import { Router, ActivatedRoute } from '@angular/router';
import { NgTemplateOutlet } from '@angular/common';
import { ConfiremDialogComponent } from '../../../shared/confirm-dialog/confirm.component';

@Component({
    selector: 'product-category-list',
    templateUrl: './product-category-list.component.html',   
})

export class ProductCategoryListComponent implements OnInit, AfterViewInit {
    @ViewChild('nameTpl', { static: true }) private nameTpl: TemplateRef<any>;
    // =============>> For Searching
    public key:string = ""; 
    public isSearching:boolean = false; 
    data: any[] = [];
    total: number;
    limit: number = 50;
    page:   number = 1;
    public setup:any;
    role:number     = 0 ;
    location:number = 0 ;

   
    /** end template for table column */

    tableColumnInfo: TableColumnInfo[];

    constructor(
        private service: Service,
        private _snackBar: MatSnackBar,
        private dialog: MatDialog,
        private activatedRoute: ActivatedRoute,
        private router: Router,

    ) { }

    // tslint:disable-next-line:typedef
    ngOnInit() {
        this.getQueryParams();
        this.listing(this.limit, this.page);
        this._initTableInfo(); 
        this.setupFn();
    }

    ngAfterViewInit() {
    }

    setupFn(){
        this.service.setup().subscribe(res => {
            this.setup = res;
        });
    }

    /**
     * Get product list
     */
    // tslint:disable-next-line:typedef
    listing(limit: number = 10, page: number = 1) {
        
        this.isSearching = true; 
        let x:any = {
            limit: limit,
            page: page
        }

        if(this.role != 0){
            x.role = this.role;
        }

        if(this.location != 0){
            x.location = this.location;
        }

        if(this.key != ""){
            x.key = this.key; 
        }
        this.router.navigate([`product-categories`], { queryParams: x});

        this.service.listing(x).subscribe(res => {

            // this.isSearching = false; 
            // this.data = res.data;
            // this.total = res.meta.pagination.total;
            // this.page  = res.meta.pagination.current_page;
            // this.limit = res.meta.pagination.per_page;

        });
    }

    goDetail(row){
        this.router.navigate([`product-categories/`+row.id+`/overview`]);
    }

    getQueryParams(){
        this.activatedRoute.queryParams.subscribe(params => {
            this.limit = params['limit']? params['limit'] : 50;
            this.page = params['page']? params['page'] : 1;
            this.key = params['key']? params['key'] : '';
        });
    }

    // tslint:disable-next-line:typedef
    onSort(sort: MatSort) {
        //this.listing();
    }

    onPageChanged(event) {
        if (event && event.pageSize) {
            this.limit = event.pageSize;
            this.page = event.pageIndex + 1;
            this.listing(this.limit, this.page);
        }

    }

    // tslint:disable-next-line:typedef
    console(item?) {
    }

    private _initTableInfo():void {
        this.tableColumnInfo = [
            {
                key: 1,
                label: 'ឈ្មោះផលិតផល',
                name: 'name',
                sortable: true,
                template: this.nameTpl,
               
            },
            
        ];
    }

    onDelete(element:any){
        const dialogRef = this.dialog.open(ConfiremDialogComponent);
        dialogRef.afterClosed().subscribe((result) => {
          if(result){
           
            this.service.delete(element.id).subscribe(
              (response) => {
 
                if(response.status === 'success'){
                //   const position = this.data.findIndex(
                //     (item: any) => {
                //       return item.id == element.id;
                //     }
                //   );
                //   this.data.splice(position, 1);
                this.listing(this.limit, this.page);
                  this._snackBar.open(response.message, 'សារ',{verticalPosition:'bottom', horizontalPosition:"right", duration:5000, panelClass: ['green-snackbar']});
                }
              }
            );
          }
        });
    }

}


