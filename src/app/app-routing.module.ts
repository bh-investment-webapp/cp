import { ProjectDashboardModule } from './main/apps/dashboards/project/project.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';


const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'my-profile',
    loadChildren: () => import('./main/my-profile/my-profile.module').then(m => m.MyProfileModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./main/dashboard/dashboard.module').then(m => m.DashboardModule),
    canLoad: [AuthGuard]
  },

  {
    path: 'users',
    loadChildren: () => import('./main/user/user.module').then(m => m.UserModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'sales',
    loadChildren: () => import('./main/sale/sale.module').then(m => m.SaleModule),
    canLoad: [AuthGuard]
  },
  { 
    path: 'products', 
     loadChildren: () => import('./main/product/product.module').then(m => m.ProductModule),
     canLoad: [AuthGuard]
  },
  { 
    path: 'product-categories', 
     loadChildren: () => import('./main/product-category/product-category.module').then(m => m.ProductCategoryModule),
     canLoad: [AuthGuard]
  },
  {
    path: '**',
    loadChildren: () => import('./main/my-profile/my-profile.module').then(m => m.MyProfileModule),
    canLoad: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    {useHash: true}
    // { enableTracing: true } // <-- debugging purposes only
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
