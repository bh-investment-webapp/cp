import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ProductCategoryService {
    
    public id  = 0;
    public data: any; 


    httpOptions = {
        headers: new HttpHeaders({
            'Conten-type': 'application/json',
            'withCredentials': 'true',
        })
    };

    constructor(private http: HttpClient) { }

    // tslint:disable-next-line: typedef
    setId(id = 0){
        this.id = id;
    }
    // tslint:disable-next-line: typedef
    getId(){
        return this.id;
    }
   
    /**
     * Get List category using GET HTTP client
     */
    listing(param_from_component): Observable<any> {
        // TODO: send the message _after_ fetching the heroes
        // this.messageService.add('HeroService: fetched heroes');
        const httpOptions = {};
        let params = new HttpParams();
        params = param_from_component;
        httpOptions['params'] = params;


        return this.http
            .get<any>('/cp/setups/product-category', httpOptions)
            .pipe(
                tap(_ => { }),
                catchError(this.handleError<any>('getCategory', []))
            );
    }

    // =============== Get Category

    view(id:string = ''): Observable<any> {
        return this.http
            .get<any>('/cp/setups/product-category/'+id)
            .pipe(
                tap(_ => { }),
                catchError(this.handleError<any>('get1Category', []))
            );
    }

    // =============== Create Category of product
    create(body: object): Observable<any> {
        return this.http
            .post('/cp/setups/product-category', body, this.httpOptions)
            .pipe(
                tap(_ => console.log('creating new Category of product')),
                catchError(this.handleError<any>('Cannot create Category', []))
            );
    }

    action(type:string = "", id:string = "", body:object): Observable<any> {
        if(type == "CREATE"){
            return this.http
            .post('/cp/setups/product-category', body, this.httpOptions);
        }else{
            return this.http
            .put('/cp/setups/product-category'+ id, body, this.httpOptions);
        }
    }

    // ============= Update product
    updateproduct( id:number = 0, data: {}): Observable<any> {
        return this.http
            .put('/cp/setups/product-category/' + id + '?_method=PUT', data)
            .pipe(
                tap(_ => console.log('updating new product')),
                catchError(this.handleError<any>('Cannot update category', []))
            );
    }

    delete(id: number = 0): Observable<any> {
        return this.http
            .delete<any>('/cp/setups/product-category/'+ id)
            .pipe(
                tap(_ => { }),
                catchError(this.handleError<any>('HaHa!, You Can not delete the product!', []))
            );
    }

    // Get Setup date
    setup(): Observable<any> {
        return this.http
            .get<any>('/cp/setups/product-category')
            .pipe(
                tap(_ => { }),
                catchError(this.handleError<any>('getcategory', []))
            );
    }
    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for product consumption
            console.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
