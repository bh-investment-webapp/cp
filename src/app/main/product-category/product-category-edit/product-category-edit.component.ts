import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import { ValidatorService } from 'app/shared/validator/validator.service';
import { ProductCategoryService as Service } from '../product-category.service';
import { MyDateAdapter, MyDateProvider } from 'app/shared/format/date.format';

import { ActivatedRoute } from '@angular/router';

@Component({
    
    templateUrl: './product-category-edit.component.html',
    providers: [MyDateAdapter, MyDateProvider]

})
export class ProductCategoryEditComponent implements OnInit {

    // tslint:disable-next-line: ban-types
    hidePassword = true;
    public setup: any;
    public id  = 0;
    public data: any;
    public code  = '';
    public isLoading = false;
    public form: FormGroup;
    public validationData:any; 
    public src  = 'assets/mpwt/person-placeholder.jpg'; 
    public minDate: Date;
    public maxDate: Date;

    constructor(
        private service: Service,
        private validatorService: ValidatorService,
        private _formBuilder: FormBuilder,
        private route: Router,
        private _snackBar: MatSnackBar,
        private router: ActivatedRoute, 
    ) { 
        this.router.paramMap.subscribe(params => {
            this.code = params.get('id');
          })
    }

    ngOnInit(): void {
     this.getData();
       
    }
 
    // tslint:disable-next-line: typedef
    getData(){
        this.isLoading = true;
        this.service.view(this.code).subscribe(
            (response) => {
                this.isLoading = false;
                this.data = response;
            }
          );
    }

    // tslint:disable-next-line: typedef
    getSetupData(val){
        this.setup = val;
    }
   
}


