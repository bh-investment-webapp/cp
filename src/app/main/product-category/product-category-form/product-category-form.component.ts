import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import { ValidatorService } from 'app/shared/validator/validator.service';
import { ProductCategoryService as Service } from '../product-category.service';
import { MyDateAdapter, MyDateProvider } from 'app/shared/format/date.format';

import { FunctionService } from '../../../../app/helper/function.service';

@Component({
    selector: 'product-category-form',
    templateUrl: './product-category-form.component.html',
    providers: [MyDateAdapter, MyDateProvider]

})
export class ProductCategoryFormComponent implements OnInit {
    @Output()
    emitSetupData = new EventEmitter<string>();
    hidePassword: Boolean = true;

    public mode:any;
    public isLoading: Boolean = false;
    public form: FormGroup;
    public validationData:any; 
    public setup: any;
    public minDate: Date;
    public maxDate: Date;
    public category: any;
    @Input() data:any = {};
    @Input() action:string = "CREATE";

    constructor(
        private service: Service,
        private validatorService: ValidatorService,
        private _formBuilder: FormBuilder,
        private route: Router,
        private _snackBar: MatSnackBar,
        public fs: FunctionService
    ) { 
    }

    ngOnInit(): void {
        this._buildForm();
        this.setupFn();
    }

    setupFn(){
        this.service.setup().subscribe(res => {
            this.setup = res;
            this.emitSetupData.emit(this.setup);
        });
    }
 
    submit(){
        if(this.form.valid){

            this.isLoading = true;
            
            this.service.action(this.action, this.data ? this.data.id : '', this.form.value).subscribe(res => {
                this.isLoading = false;
                this._snackBar.open(res.message, 'សារ',{verticalPosition:"bottom", horizontalPosition:"right", duration:5000, panelClass: ['green-snackbar']});
                this.route.navigate([`product-categories/`+res.data.id+'/overview']);
              }, err => {
                  this.isLoading = false;
                  // tslint:disable-next-line: forin
                  for (const key in err.error.errors){
                    const control = this.form.get(key);
                    control.setErrors({servererror: true});
                    control.errors.servererror = err.error.errors[key][0];
                  }
              });
        }else{
            this._snackBar.open('សូមបំពេញព័ត៌មានឪ្យបានត្រឹមត្រូវ', 'សារ',{verticalPosition:"bottom", horizontalPosition:"right", duration:5000,  panelClass: ['red-snackbar']});
        }
        
    }

    // tslint:disable-next-line: typedef
    private _buildForm() {
        this.form = new FormGroup({
            name: new FormControl(this.data ? this.data.name : '', []),
        });
    }

   

   
   
}


