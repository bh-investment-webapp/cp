import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductCategoryListComponent as ListComponent } from './product-category-list/product-category-list.component';
import { ProductCategoryCreateComponent as CreateComponent } from './product-category-create/product-category-create.component';
import { ProductCategoryComponent as MainComponent } from './product-category.component';
import { ProductCategoryEditComponent as EditComponent} from './product-category-edit/product-category-edit.component';

const routes: Routes = [
    {
        path: '', component: MainComponent,
        children: [
            {
                path: '',
                // canActivateChild: [AuthGuard],
                children: [
                    { path: '', component: ListComponent },
                    { path: 'create', component: CreateComponent },
                    { path: ':id/overview', component: EditComponent },

                ]
            }
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ProductCategoryRoutingModule { }
