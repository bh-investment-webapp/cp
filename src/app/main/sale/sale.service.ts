import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
// import { Staff } from './staff';
// import { STAFFS } from 'app/mock/mock-staff';
import { tap, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class MyProfileService {
    httpOptions = {
        headers: new HttpHeaders({
            'Conten-type': 'application/json',
            'withCredentials': 'true',
        })
    };

    constructor(private http: HttpClient) { }


    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
}
