import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDividerModule } from '@angular/material/divider';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMenuModule } from '@angular/material/menu';
import { MatSlideToggleModule} from '@angular/material'

import {
  FuseCountdownModule,
  FuseWidgetModule,
  FuseHighlightModule,
  FuseProgressBarModule,
  FuseShortcutsModule
} from '@fuse/components';
import {MatDialogModule,  } from '@angular/material/dialog';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { SharedModule } from 'app/shared/shared.module';

import { ProductCategoryRoutingModule as RoutingModule } from './product-category.routing.module';
import { ProductCategoryComponent as MainComponent } from './product-category.component';
import { ProductCategoryListComponent as ListComponent } from './product-category-list/product-category-list.component';
import { ProductCategoryEditComponent as EditComponent} from './product-category-edit/product-category-edit.component';
import { ProductCategoryCreateComponent as CreateComponent } from './product-category-create/product-category-create.component';
import { ProductCategoryFormComponent  } from './product-category-form/product-category-form.component';


//NgxPagination
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module

@NgModule({
  declarations: [

    MainComponent,
    ListComponent,
    EditComponent,
    CreateComponent,
    ProductCategoryFormComponent,

  ],
  imports: [
    RoutingModule,
    TranslateModule,
    MatProgressSpinnerModule, 
 
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatSortModule,
    MatFormFieldModule,
    MatSlideToggleModule,
    MatProgressBarModule,
    MatDividerModule,
    MatRadioModule,
    MatDatepickerModule,
    MatMenuModule, 

    FuseSharedModule,
    FuseCountdownModule,
    FuseWidgetModule,
    FuseHighlightModule,
    FuseProgressBarModule,
    FuseShortcutsModule,
    SharedModule,
    NgxPaginationModule,
    MatTabsModule,
    MatTableModule,
    MatCheckboxModule,
    MatDialogModule,
  ],
  entryComponents: [

  ],
  exports: [
   
  ]
})
export class ProductCategoryModule { }
