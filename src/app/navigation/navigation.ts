import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
   
    
    {
        id       : 'applications',
        title    : 'Menus',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        icon     : 'apps',
        children : [
            {
                id       : 'dashboard',
                title    : 'Dashboards',
                translate: 'NAV.DASHBOARD',
                type     : 'item',
                icon     : 'receipt',
                url      : '/dashboard',
                permission: '',
                children : [],
            }, 

            {
                id       : 'sale',
                title    : 'Sales',
                translate: 'NAV.SALES',
                type     : 'item',
                icon     : 'monetization_on',
                url      : '/sales',
                permission: '',
                children : [],
            },
            
            {
                id       : 'product',
                title    : 'Product',
                translate: 'Product',
                type     : 'item',
                icon     : 'monetization_on',
                url      : '/products',
                permission: '',
                children : [],
            },
            
            {
                id       : 'category',
                title    : 'Category',
                translate: 'Category',
                type     : 'item',
                icon     : 'monetization_on',
                url      : '/product-categories',
                permission: '',
                children : [],
            },
            
            {
                id       : 'my-profile',
                title    : 'My Profile',
                translate: 'NAV.PROFILE',
                type     : 'item',
                icon     : 'person',
                url      : '/my-profile',
                permission: ''
            },
        ],
    },
   
  
   
 
    // {
    //     id       : 'Setup',
    //     title    : 'កំណត់',
    //     translate: 'NAV.SETUP',
    //     type     : 'collapsable',
    //     icon     : 'settings',
    //     children : [
          
    //         {
    //             id   : 'setting-security-role',
    //             title: 'ក្រុមសិទ្ធ',
    //             type : 'item',
    //             translate: 'NAV.setting-security-role',
    //             url  : '#'
    //         },     
         
        
    //     ]
    // },
    
    // {
    //     id       : 'User',
    //     title    : 'អ្នកប្រើប្រាស់',
    //     translate: 'NAV.USER',
    //     type     : 'item',
    //     icon     : 'supervised_user_circle',
    //     url      : '/users',
    //     permission: ''
    // },
 
];
